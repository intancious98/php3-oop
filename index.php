<?php

require_once("animal.php");
require_once("ape.php");
require_once("frog.php");


$sheep = new animal("shaun");

echo "Nama : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded . "<br>";

echo "<br>";

$kodok = new frog("buduk");
echo "Nama : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : ";
$kodok->jump();

echo "<br>";
echo "<br>";

$sungkong = new ape("kera sakti");

echo "Nama : " . $sungkong->name . "<br>";
echo "Legs : " . $sungkong->legs . "<br>";
echo "Cold Blooded : " . $sungkong->cold_blooded . "<br>";
echo "Yell : ";
$sungkong->yell();
